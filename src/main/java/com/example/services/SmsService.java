package com.example.services;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Calendar;
import java.util.UUID;


/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@Path("/sms")
public class SmsService {

  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public String respond(@QueryParam("text") String text, @QueryParam("from") String from) {
    String uuid = UUID.randomUUID().toString();
    Calendar cal = Calendar.getInstance();
    int day = cal.get(Calendar.DATE);
    int month = cal.get(Calendar.MONTH) + 1;
    int year = cal.get(Calendar.YEAR);
    int dow = cal.get(Calendar.DAY_OF_WEEK);
    int dom = cal.get(Calendar.DAY_OF_MONTH);
    int doy = cal.get(Calendar.DAY_OF_YEAR);
    //String url = "http://sent.ly/command/sendsms?username=[alexbutum@gmail.com]&password=[brasovbus]&to=[+40765625471]&text=" + "[" + message + "]";
    /*DefaultHttpClient client = new DefaultHttpClient();
    HttpGet request = new HttpGet(url);
    try {
      HttpResponse response = client.execute(request);
    }
    catch (IOException e) {
      e.printStackTrace();
    }*/

    return "Your generated ticket ID is : " + uuid.replaceAll("-", "") + " and is valid for " + day + "_" + month + "_" + year
        + "_" + (from != null ? from : "++40765625471");
  }

}
